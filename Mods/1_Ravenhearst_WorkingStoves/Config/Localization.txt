﻿Key,Source,Context,English
stoveKitRH,items,Item,Stove Repair Kit
stoveKitRHDesc,items,Item,"This stove repair kit is used with a hammer to take a broken stove and make it a working one that you can use."
workingStoveRH,blocks,Block,Working Stove
workingStoveRHDesc,blocks,Block,"A working stove can be used to cook food and some chemicals in a safe environment."