=== BOID'S POUR ONE OUT MODLET
=== v1.0, 9/10/2020

WHAT THIS MOD DOES
- Allows you to right-click on any terrain block with a jar of Murky Water to turn it into an empty jar

INSTALLATION
- Unzip into the /mods folder so you will have:

<7D2D install folder>/mods/BoidsPourOneOut
   ModInfo.xml
   /Config
     items.xml
     
HOW TO USE IN GAME
- Put Murky Water jar(s) into active toolbelt slot
- Right-click on water or any solid terrain (dirt, snow, asphalt) to empty the jar(s)
- Does NOT add water into the terrain (i.e. cannot be used to create a puddle or refill 'holes' in water)