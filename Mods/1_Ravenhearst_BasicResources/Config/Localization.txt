﻿Key,Source,Context,English
resourceFiberCordageRH,items,Item,Fiber Cordage,,,,,
resourceFiberCordageRHDesc,items,Item,"Cordage can be used to bind together primitive items.",,,,,
resourceArrowHeadStoneRH,items,Tool,Stone Arrowhead,,,,,
resourceArrowHeadStoneRHDesc,items,Tool,"Crafted from stone, used to make arrows and bolts.",,,,,
resourceArrowShaftRH,items,Tool,Arrow Shaft,,,,,
resourceArrowShaftRHDesc,items,Tool,"Crafted from wood, arrow shafts are necessary to make arrows.",,,,,
resourceStickRH,items,Item,Sticks,,,,,
resourceStickRHDesc,items,Item,"Crafted from wood, sticks are useful in many survival situations.",,,,,
resourcePlankRH,items,Item,Plank,,,,,
resourcePlankRHDesc,items,Item,"Crafted from wood, planks are used in building.",,,,,
resourceTwineRH,items,Item,Twine,,,,,
resourceTwineRHDesc,items,Item,"Use to craft clothing, armor, and weapons.",,,,,
resourceSharpenedStoneRH,items,Item,Sharp Stone,,,,,
resourceSharpenedStoneRHDesc,items,Item,"Sharpening the stones you find helps you make deadly tools and weapons.",,,,,
