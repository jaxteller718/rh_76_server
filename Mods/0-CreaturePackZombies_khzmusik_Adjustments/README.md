# 0-CreaturePackZombies_khzmusik_Adjustments

Adjustments for the `0-CreaturePackZombies` modlet.

A lot of these adjustments are for balance reasons,
and/or removing zombies for thematic or aesthetic reasons.
Much of this is personal taste.
YMMV.

I kept notes about the zombies in `NOTES.md`.
That file is included in this repo, if you want to know my reasoning.

## Changes

These zombies no longer spawn:

* `zombieGuppyKennethClown`
* `zombieNurseTemplate`
* `zombieSanta`
* `zombieWhiteClown`
* `zombieGuppyBiomechanicalWight`
* `zombieGuppyCreepyCrawly`
* `zombieGuppyInfernalDog`
* `zombieRekt` (all variations)
* `zombieHugh` (all variations)
* `zombieGuppyOldManZombie`
* `zombieGuppyProstitute`
* `zombieGuppyMalePatient`
* `zombieGuppySoldier01` (all variations)
* `zombieGuppySoldierFemale01` (all variations)
* `zombieGuppyDoctor` (all variations)

For the remaining zombies, these changes were made:

* The remaining soldier zombies spawn with `zombieSoldier`
* `zombieGuppyPest` spawns with `zombieBurnt` and `zombieSteveCrawler`;
  it also is no longer a brute, and uses the `zombieSteveCrawler` sounds
* `zombieGuppyAbonimation` _\[sic]_ spawns with `zombieFatHawaiian`

Several changes were made to `zombieBehemoth`:
* it now spawns only in blood moon hordes
* it is smaller, so it can fit through a 2x3-block opening;
  otherwise the AI never breaks enough blocks to reach the player
* its melee reach is adjusted to match its new size (values based on the bear)
* it uses a mix of bear and demolition zombie sounds

## Dependent and Compatible Modlets

This modlet is prefixed with a number, in order to load it after all dependent modlets.

This modlet is dependent upon the `0-CreaturePackZombies` modlet.
It was designed and tested to be used with version `19.0.3.41` of that modlet.
It *might* not work with older or newer versions of that modlet.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients,
so separate client installation should not be necessary.

However, the `0-CreaturePackZombies` modlet also includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, that modlet should be installed on both servers and clients.
